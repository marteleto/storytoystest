﻿/*
 * Makes the camera follow
 * specifc target while keeping
 * distance
 */
using UnityEngine;

public class FollowCamera : MonoBehaviour
{
    public Transform target;
    public Vector3 targetDistance = new Vector3(0.0F, 0.0F, -20.0F);
    public float moveSpeed = 1.0F;
    public float rotationSpeed = 6.0F;

    private void Start()
    {
        setupDistance();
    }

    void Update()
    {
        updateMovement();
    }

    // Calculates offset distance
    private void setupDistance()
    {
        targetDistance = transform.position - target.position;
    }

    // Updates movement and rotation
    // applying linear interpolation
    private void updateMovement()
    {
        if (!target) return;
        transform.position = Vector3.Lerp(transform.position, target.position + targetDistance, moveSpeed * Time.deltaTime);
        Vector3 relativePosition = target.position - transform.position;
        Quaternion lookAtRotation = Quaternion.LookRotation(relativePosition);
        Quaternion targetRotation = Quaternion.Euler(lookAtRotation.eulerAngles.x, lookAtRotation.eulerAngles.y, 0.0F);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
    }
}
