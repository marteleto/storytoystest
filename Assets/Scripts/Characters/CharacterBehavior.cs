﻿/*
 * Controls movement and touch controls
 * Can be divided into separate components
 * to facilitate scale and maintenance
 */
using UnityEngine;

[RequireComponent(typeof(CharacterController))]

public class CharacterBehavior : MonoBehaviour
{
    public float moveSpeed = 200.0F;
    public float rotationSpeed = 1.0F;
    public float minTargetDistance = 2.0F;

    private Vector3 targetPosition;
    private CharacterController controller;
    private float currentTargetDistance;

    void Start()
    {
        setupController();
        setupDefaultPosition();
    }

    void Update()
    {
        updateMouseControls();
        updateTouchControls();
        updateMovement();
        updateEvents();
    }

    //Stores reference to use every frame
    private void setupController()
    {
        controller = GetComponent<CharacterController>();
    }

    private void setupDefaultPosition()
    {
        targetPosition = transform.position;
    }

    // Mouse controls to help debug movement on PC
    private void updateMouseControls()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            Debug.DrawRay(ray.origin, ray.direction * 100.0F, Color.red);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                targetPosition = hit.point;
            }
        }
    }

    // Touch controls
    // Only runs if there are active touches
    // Casts a ray from camera to scenario
    // Once it hits, updates global variable targetPosition to hit point
    private void updateTouchControls()
    {
        bool hasActiveTouches = Input.touchCount > 0;
        if (hasActiveTouches)
        {
            int lastTouchIndex = Input.touchCount - 1;
            Vector2 screenTouchPosition = Input.touches[lastTouchIndex].position;
            Ray ray = Camera.main.ScreenPointToRay(screenTouchPosition);
            if (Physics.Raycast(ray, out RaycastHit hit))
            {
                targetPosition = hit.point;
            }
        }
    }

    // Updates character movement
    // Only runs if the distance from character
    // to target is greater than minTargetDistance
    // Rotates character to the direction of target
    // Always moves foward
    private void updateMovement()
    {
        currentTargetDistance = Vector3.Distance(targetPosition, transform.position);
        bool shouldMove = currentTargetDistance > minTargetDistance ? true : false;
        if (!shouldMove) return;

        Vector3 relativePosition = targetPosition - transform.position;
        Quaternion lookAtRotation = Quaternion.LookRotation(relativePosition);
        Quaternion targetRotation = Quaternion.Euler(0.0F, lookAtRotation.eulerAngles.y, 0.0F);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);

        Vector3 forward = transform.TransformDirection(Vector3.forward);
        controller.SimpleMove(forward * moveSpeed * Time.deltaTime);
    }

    private void updateEvents ()
    {
        bool isWalking = currentTargetDistance > minTargetDistance;
        sendWalkingEvent(isWalking);
    }

    private void sendWalkingEvent(bool isWalking)
    {
        gameObject.BroadcastMessage("characterWalkingEvent", isWalking, SendMessageOptions.DontRequireReceiver);
    }
}
