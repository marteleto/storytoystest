﻿using UnityEngine;

public class CharacterPhysics : MonoBehaviour
{
    private readonly float pushPower = 2.0F;
    private readonly float weight = 6.0F;

    void OnControllerColliderHit(ControllerColliderHit hit)
    {
        Vector3 force;
        Rigidbody rigidbody = hit.collider.attachedRigidbody;

        if (rigidbody == null || rigidbody.isKinematic) return;

        if (hit.moveDirection.y < -0.3)
        {
            force = new Vector3(0, -0.5F, 0) * 9.81F * weight;
        }
        else
        {
            force = hit.controller.velocity * pushPower;
        }

        rigidbody.AddForceAtPosition(force, hit.point);
    }
}
