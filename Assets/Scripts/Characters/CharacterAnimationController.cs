﻿/*
 * Controls animation states
 * from external events and
 * keyboard events
 */
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
    private Animator animator;
    private bool isWalking;

    void Start()
    {
        setupAnimator();
    }

    void Update()
    {
        updateAnimationState();
        updateKeyboardEvent();
    }

    private void setupAnimator()
    {
        animator = GetComponent<Animator>();
    }

    private void updateKeyboardEvent()
    {
        isWalking = Input.GetKey("left") || Input.GetKey("right") ? true : false;
    }

    private void updateAnimationState()
    {
        animator.SetBool("Walking", isWalking);
    }

    public void characterWalkingEvent(bool isWalking)
    {
        this.isWalking = isWalking;
    }

}
